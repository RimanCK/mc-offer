import asyncio

from offers.app.sanic import create_app

app = create_app()

if __name__ == '__main__':
    # loop = asyncio.get_event_loop()
    # srv = loop.run_until_complete(app)
    # try:
    #     assert srv.is_serving() is False
    #     loop.run_until_complete(srv.start_serving())
    #     assert srv.is_serving() is True
    #     loop.run_until_complete(srv.serve_forever())
    # except KeyboardInterrupt:
    #     loop.run_until_complete(srv.close())
    #     loop.close()
    app.run(host="0.0.0.0", port=5000)
