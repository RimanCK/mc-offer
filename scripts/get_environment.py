import os


def get_environment():
    return (os.environ.get('ENVIRONMENT'))


if __name__ == '__main__':
    print(get_environment())
