__all__ = [
    'exception_handler',
    'map_db_to_model_fieldname',
    'select_as_model_fieldnames',
]

from sanic.response import json
from sqlalchemy import inspect

from utils.exceptions import CUSTOM_EXCEPTIONS, CustomException

DEFAULT_STATUS = 500


def exception_handler(request, exception: Exception):
        exception_class = CUSTOM_EXCEPTIONS.get(exception.__class__)
        exception = exception_class(message=str(exception)) if exception_class else exception
        status = getattr(exception, 'status', None) or DEFAULT_STATUS
        return json(
            {
                'description': str(exception),
                'status': status,
            },
            status=status,
        )


def map_db_to_model_fieldname(model) -> dict:
    return {
        v.expression.key: field_name
        for field_name, v in inspect(model).mapper.columns.items()
    }


def select_as_model_fieldnames(model, only: list = None) -> list:
    return [
        getattr(model, model_fieldname).label(model_fieldname)
        for model_fieldname in map_db_to_model_fieldname(model).values()
        if not only or (only and model_fieldname in only)
    ]
