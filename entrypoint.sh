#!/bin/bash

export ENVIRONMENT=$(python scripts/get_environment.py)

exec gunicorn wsgi:app --bind 0.0.0.0:5000 --worker-class sanic.worker.GunicornWorker