import pytest

from offers.app.sanic import create_app


@pytest.yield_fixture
def app():
    app = create_app()
    yield app


@pytest.fixture
def client(loop, app, sanic_client):
    return loop.run_until_complete(sanic_client(app))
