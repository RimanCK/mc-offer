from marshmallow import (
    Schema,
    fields,
    EXCLUDE,
    pre_dump,
)


class OfferSchema(Schema):
    title = fields.String(required=True)
    text = fields.String(required=True)

    @pre_dump
    def remove_skip_values(self, data, **kwargs):
        return {key: value for key, value in data.items() if value}


offer_response = OfferSchema(many=True, unknown=EXCLUDE)
