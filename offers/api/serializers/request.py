from marshmallow import (
    Schema,
    fields,
    EXCLUDE,
    ValidationError,
    validates_schema,
)


class OfferCreateSchema(Schema):
    user_id = fields.Integer(required=True)
    title = fields.String(required=True)
    text = fields.String(required=True)


class OfferGetSchema(Schema):
    offer_id = fields.Integer()
    user_id = fields.Integer()

    @validates_schema
    def validator(self, data, **kwargs):
        if not (data.get('offer_id') or data.get('user_id')):
            raise ValidationError('Should be offer_id or user_id!')


create_offer_request = OfferCreateSchema(unknown=EXCLUDE)
offer_get_request = OfferGetSchema(unknown=EXCLUDE)
