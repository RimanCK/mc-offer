from sanic import Blueprint, response
from sanic.request import Request

from offers.api.serializers import (
    create_offer_request,
    offer_get_request,
)
from offers.api.serializers import (
    offer_response,
)
from offers.api.v1 import controllers

v1_blueprint = Blueprint('my_blueprint')


@v1_blueprint.route('offer/create', methods=['POST'])
async def create_offer(request: Request):
    data = create_offer_request.load(request.json)
    await controllers.create_offer(**data)
    return response.HTTPResponse(status=201)


@v1_blueprint.route('offer/', methods=['POST'])
async def get_offer(request: Request):
    data = offer_get_request.load(request.json)
    offers = await controllers.get_offers(**data)
    return response.json(offer_response.dump(offers))
