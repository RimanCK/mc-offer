from sqlalchemy import select, insert, or_
from offers.api.models import Offer
from offers.app.db import db_session
from utils import select_as_model_fieldnames


@db_session
async def create_offer(
        user_id: int,
        title: str,
        text: str,
        db_session,
):
    await db_session.execute(
        insert(Offer).values(
            {
                Offer.user_id: user_id,
                Offer.title: title,
                Offer.text: text,
            },
        ),
    )


@db_session
async def get_offers(
        db_session,
        user_id: int = None,
        offer_id: int = None,
):
    offers = await db_session.fetch(
        select([*select_as_model_fieldnames(Offer)]).select_from(Offer).
            where(or_(Offer.user_id == user_id, Offer.id == offer_id)))

    return offers
