from sanic import Sanic

from offers.api.v1.views import v1_blueprint
from offers.app.db import DBPool
from offers.app.logging import init_logging
from utils import exception_handler


def create_app():
    init_logging()

    app = Sanic(__name__)
    app.blueprint(
        [
            v1_blueprint,
        ],
    )
    app.register_listener(DBPool.init_db, 'before_server_start')
    app.error_handler.add(Exception, exception_handler)

    return app
