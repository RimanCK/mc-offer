import logging.config
import os

from .config import config, Environment

LOGGING_CONFIG = {
    'version': 1,
    'formatters': {
        'default': {
            'format': ('%(levelname)s::%(asctime)s:%(name)s.'
                       '%(funcName)s:%(message)s'),
            'datefmt': '%Y-%m-%d %H:%M:%S',
        },
    },
    'handlers': {
        'console': {
            'level': config.LOG_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'default',
            'stream': 'ext://sys.stdout',
        },
        'file': {
            'level': config.LOG_LEVEL,
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'default',
            'filename': os.path.join(os.path.abspath('./logs'), 'log.txt'),
            'backupCount': 0,
            'maxBytes': 1024 * 1024 * 5,
        },
    },
    'loggers': {
        config.APP_NAME: {
            'level': config.LOG_LEVEL,
            'handlers': (['console', 'file']),
        },
    },
    'disable_existing_loggers': False,
}


def init_logging():
    if config.ENVIRONMENT in [
        # Environment.PROD,
    ]:
        LOGGING_CONFIG['loggers'][config.APP_NAME]['handlers'].remove('console')

    logging.config.dictConfig(LOGGING_CONFIG)
