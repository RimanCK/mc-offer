import os
from config_parser import BaseConfig


ENVIRONMENT = os.environ['ENVIRONMENT'].upper()
CONFIG_FILE = os.environ.get('CONFIG_FILE', os.path.join('config', f'{ENVIRONMENT.lower()}.yml'))


class Environment:
    DEV = 'DEV'
    TESTING = 'TESTING'
    PROD = 'PROD'


class ConfigApp(BaseConfig):
    INIT_LOGGING: bool = True
    DEBUG: bool = False

    ENVIRONMENT: str
    APP_NAME: str

    LOG_LEVEL = 'INFO'

    DB_PG_NAME: str
    DB_PG_USERNAME: str
    DB_PG_PASSWORD: str
    DB_PG_HOST: str
    DB_PG_PORT: int = 5432
    DB_SCHEMA: str

    EXPIRE_JWT_TOKEN: int

    @property
    def DB_URL(self):
        return 'postgresql://{user}:{password}@{host}:{port}/{name}'.format(
            user=self.DB_PG_USERNAME,
            password=self.DB_PG_PASSWORD,
            host=self.DB_PG_HOST,
            port=self.DB_PG_PORT,
            name=self.DB_PG_NAME,
        )


class DevConfig(ConfigApp):
    pass


class TestingConfig(ConfigApp):
    pass


class ProdConfig(ConfigApp):
    pass


config_class = {
    Environment.DEV: DevConfig,
    Environment.TESTING: TestingConfig,
    Environment.PROD: ProdConfig,
}.get(ENVIRONMENT.upper(), BaseConfig)


config: ConfigApp = config_class.from_file(CONFIG_FILE)
